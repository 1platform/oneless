#!/usr/bin/env node
const fs = require("fs");

const pkg = JSON.parse(fs.readFileSync("./package.json", "utf-8"));

const newPkg = {
  name: "@1platform/oneless",
  version: pkg.version,
  packageManager: pkg.packageManager,
  description: pkg.description,
  bin: {
    oneless: "./cli.js",
  },
  module: "index.js",
  types: "index.d.ts",
  main: "index.js",
  repository: pkg.repository,
  keywords: pkg.keywords,
  author: pkg.author,
  license: pkg.license,
  bugs: pkg.bugs,
  homepage: pkg.homepage,
  dependencies: pkg.dependencies,
  publishConfig: {
    "@1platform:registry": "https://gitlab.com/api/v4/projects/41109425/packages/npm/"
  }
}

fs.writeFileSync(`/tmp/publish/package.json`, JSON.stringify(newPkg, null, 2), "utf-8");
