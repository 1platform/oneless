# Oneless Changelog

Here we will note all the changes made to the Oneless Serverless Framework.

## Version 0.1.0

Initial application release.
