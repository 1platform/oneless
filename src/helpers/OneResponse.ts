import HTTPStatus from "../utils/HTTPStatus";
import IOneResponse from "../types/IOneResponse";

export default class OneResponse<T = any> {
  private _status: HTTPStatus = HTTPStatus.OK;

  public get status(): HTTPStatus {
    return this._status;
  }

  public set status(status: HTTPStatus) {
    this._status = status;
  }

  private _body: T = {} as any;

  public get body(): T {
    return this._body;
  }

  public set body(body: T) {
    this._body = body;
  }

  public toJSON(): IOneResponse<T> {
    return {
      status: this.status,
      body: this.body,
    };
  }
}
