import OneResponse from "./OneResponse";
import HTTPStatus from "../utils/HTTPStatus";

export { OneResponse, HTTPStatus };
