import { Arguments, Argv, CommandModule } from "yargs";
import AppLogger from "../utils/Logger";
import OneServer from "../engine/OneServer";
import Env from "../utils/Env";
import ExtractFunctions from "../engine/ExtractFunctions";

/**
 * CLI Command used to create a new migration.
 */
export default class RunCommand implements CommandModule {
  command = "run";
  describe = "Runs the current serverless module";

  /**
   * Method used to define the options and parameters that the CLI command
   * can receive.
   *
   * @param args A list with arguments and options sent by the user to the command.
   */
  public builder(args: Argv) {
    return args.option("production", {
      alias: "p",
      type: "boolean",
      describe: "Run the application in production mode.",
    });
  }

  /**
   * Method used to run the command.
   *
   * @param args A list with arguments and options sent by the user to the command.
   */
  public async handler(args: Arguments<{ production?: boolean }>) {
    try {
      await OneServer(await ExtractFunctions(process.cwd(), !args.production), Env.int("APP_PORT", 3000));
    } catch (err) {
      AppLogger.error(err.message);
      AppLogger.silly(err.stack);
    }
  }
}
