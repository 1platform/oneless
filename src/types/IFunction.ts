import IOneContext from "./IOneContext";
import IOneRequest from "./IOneRequest";
import IOneResponse from "./IOneResponse";

export type OneFunction<TBody = any, TResponse = any> = (context: IOneContext, request?: IOneRequest<TBody>) => Promise<IOneResponse<TResponse>>;

export default interface IFunction {
  name: string;
  file: string;
  function: OneFunction;
}
