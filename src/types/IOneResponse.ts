import HTTPStatus from "../utils/HTTPStatus";

export default interface IOneResponse<T = any> {
  status: HTTPStatus;
  body: T;
}
