#!/usr/bin/env node

import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import register from "@babel/register";
import "./utils/Env";
import { getVersion } from "./version";
import RunCommand from "./commands/RunCommand";
import path from "path";
import * as fs from "fs";

const babelRCFile = path.resolve(__dirname, "./babel.config.json");
let babelRC = {};
if (fs.existsSync(babelRCFile)) {
  babelRC = JSON.parse(fs.readFileSync(babelRCFile, "utf-8"));
}
register({ ...(babelRC || {}), extensions: [ ".ts", ".js", ".mjs" ] });

const yargsBase = yargs(hideBin(process.argv)).scriptName("oneless").version(getVersion()).command(new RunCommand());
yargsBase.recommendCommands().demandCommand(1).strict().alias("v", "version").help("h").alias("h", "help").argv;
