import { existsSync, readdirSync, readFileSync, statSync } from "fs";
import { basename, extname, join, resolve } from "path";
import chokidar from "chokidar";
import type IFunction from "../types/IFunction";
import HTTPError from "./HTTPError";
import AppLogger from "../utils/Logger";
import Env from "../utils/Env";

const functions: Record<string, IFunction> = {};

export default async function ExtractFunctions(baseFolder: string, isDevMode = true): Promise<Record<string, IFunction>> {
  const packageJson = JSON.parse(readFileSync(resolve(baseFolder, "package.json"), "utf-8"));
  const onelessDefinition = {
    functions: "functions",
    searchPath: [ "src", "dist", "." ],
    ...(packageJson.oneless ?? {}),
  };

  let functionsFolderPath = "";
  for (const searchPath of onelessDefinition.searchPath) {
    const functionsPath = resolve(baseFolder, searchPath, onelessDefinition.functions);
    if (existsSync(functionsPath)) {
      functionsFolderPath = functionsPath;
      break;
    }
  }

  if (!functionsFolderPath) {
    throw new HTTPError("Unable to find a folder for your functions.");
  }

  const functionsFolder = functionsFolderPath;
  const files = readdirSync(functionsFolder);

  const functionFiles = files
    .filter((file) => [ ".ts", ".js", ".tsx", ".jsx" ].indexOf(extname(file)) >= 0)
    .map((file) => join(functionsFolder, file))
    .filter((file) => statSync(file).isFile());

  for await (const functionFile of functionFiles) {
    try {
      let FunctionModule = await import(functionFile);
      FunctionModule = FunctionModule.default || FunctionModule;
      if (!FunctionModule && typeof FunctionModule !== "function") {
        continue;
      }

      functions[functionFile] = {
        name: basename(functionFile, extname(functionFile)),
        file: functionFile,
        function: FunctionModule,
      };
    } catch (err) {
      AppLogger.error(err.message);
      AppLogger.silly(err.stack);
    }
  }

  if (isDevMode) {
    const envFile = resolve(baseFolder, ".env");
    const watcher = chokidar.watch([ ...Object.keys(functions), envFile ]);
    watcher.on("change", async (path: string) => {
      AppLogger.silly(`File: ${ path } changed.`);

      if (path.indexOf(".env") >= 0) {
        Env.reload();
        AppLogger.info(`Reloaded Environment File`);
        return;
      }

      delete require.cache[path];
      let FunctionModule = await import(path);
      FunctionModule = FunctionModule.default || FunctionModule;
      if (!FunctionModule && typeof FunctionModule !== "function") {
        return;
      }

      AppLogger.info(`Reloaded function: ${ functions[path].name }`);
      functions[path].function = FunctionModule;
    });
  }

  return functions;
}
