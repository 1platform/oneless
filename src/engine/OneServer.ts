import type { Application, NextFunction, Request, Response } from "express";
import express from "express";
import { Server } from "http";
import helmet from "helmet";
import bodyParser from "body-parser";
import cors from "cors";
import morgan from "morgan";
import { performance, PerformanceObserver } from "perf_hooks";
import { getVersion } from "../version";
import AppLogger from "../utils/Logger";
import PageNotFoundException from "./PageNotFoundException";
import { HTTPStatus } from "../helpers";
import IFunction from "../types/IFunction";
import { IOneRequest } from "../types";
import OneRequest from "./OneRequest";
import OneContext from "./OneContext";
import getPort from "get-port-cjs";
import HTTPError from "./HTTPError";

const app: Application = express();
const server = new Server(app);

app.enable("trust proxy");
app.set("version", getVersion());

app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.text());
app.use(cors({ origin: "*" }));
app.use((req: Request, res: Response, next: NextFunction) => {
  res.setHeader("X-Oneless-Version", `v${ getVersion() }`);
  next();
});

app.use(
  morgan("tiny", {
    stream: { write: (message) => AppLogger.verbose(message.trim()) },
  })
);

function errorLogger() {
  app.use((req: Request, res: Response, next: NextFunction) => {
    next(new PageNotFoundException());
  });

  app.use((error: any, req: Request, res: Response, next: NextFunction): void => {
    const status = error.status || HTTPStatus.SERVER_ERROR;
    const message = error.message;
    let { details } = error;

    if (details) {
      details = details.reduce(
        (accum, detail) => ({
          ...accum,
          [detail.path.join(".")]: `${ detail.message }${ detail.context && ` (Value: ${ detail.context.value })` }`,
        }),
        {}
      );
    } else {
      details = "";
    }

    AppLogger.error(`[HTTP ${ status }]: ${ message }`);
    AppLogger.silly(error);
    AppLogger.silly(error.stack);
    if (details) {
      AppLogger.silly(JSON.stringify(details));
    }

    res.status(status).json({
      status,
      message,
      details,
    });
    AppLogger.info(`POST ${ status } ${ req.path }`);
    AppLogger.verbose(
      JSON.stringify({
        status,
        message,
        details,
      })
    );
  });
}

process.on("uncaughtException", (err) => {
  AppLogger.error(err.message);
  AppLogger.debug(err.stack);
  process.exit(1); //mandatory (as per the Node.js docs)
});

process.on("SIGINT", () => {
  AppLogger.info(`Application closed.`);
  process.exit(1); //mandatory (as per the Node.js docs)
});

const obs = new PerformanceObserver((items) => {
  const duration = items.getEntries()[0].duration.toFixed(2);
  const memoryUsage = process.memoryUsage().heapUsed / 1024 / 1024;
  AppLogger.info(`Execution Time: ${ duration }ms. Used Memory: ${ Math.round(memoryUsage * 100) / 100 }MB`);
  performance.clearMarks();
});
obs.observe({ entryTypes: [ "measure" ] });

export default async function OneServer(functionDefinitions: Record<string, IFunction>, port = 3000): Promise<void> {
  const context = new OneContext();
  const activePort = await getPort({ port });
  const appURL = `http://localhost:${ activePort }`;

  return new Promise((resolve, reject) => {
    if (Object.keys(functionDefinitions).length === 0) {
      reject(new HTTPError("There are no functions defined to be loaded"));
      return;
    }

    for (const functionDefinitionKey of Object.keys(functionDefinitions)) {
      const functionDefinition = functionDefinitions[functionDefinitionKey];
      AppLogger.info(`POST ${ appURL }/${ functionDefinition.name }`);
      app.post(`/${ functionDefinition.name }`, async (req: Request, res: Response, next: NextFunction) => {
        performance.mark(`Start running ${ functionDefinition.name }`);
        try {
          const request: IOneRequest = new OneRequest(req);
          const response = await functionDefinition.function(context, request);

          res.status(response.status).json(response.body);
          AppLogger.info(`POST ${ response.status } /${ functionDefinition.name }`);
          AppLogger.verbose(JSON.stringify(response.body));
        } catch (err) {
          next(err);
        }
        performance.measure(`End running ${ functionDefinition.name }`, `Start running ${ functionDefinition.name }`);
      });
    }

    errorLogger();

    server.listen(activePort, () => {
      AppLogger.info(`Application started: ${ appURL }`);
      resolve();
    });

    server.on("close", () => {
      AppLogger.info(`Application closed.`);
    });
  });
}
