import type { Environment } from "../utils/Env";
import Env from "../utils/Env";
import IOneContext from "../types/IOneContext";
import type { Logger } from "../utils/Logger";
import AppLogger from "../utils/Logger";

export default class OneContext implements IOneContext {
  public get env(): Environment {
    return Env;
  }

  public get log(): Logger {
    return AppLogger;
  }

  public get(field: string, defaultValue = null): string | null {
    return this.env.get(field, defaultValue);
  }

  public int(field: string, defaultValue = 0): number {
    return this.env.int(field, defaultValue);
  }

  public number(field: string, defaultValue = 0): number {
    return this.env.number(field, defaultValue);
  }

  public boolean(field: string): boolean {
    return this.env.boolean(field);
  }

  public array(field: string, separator = ",", defaultValue = ""): string[] {
    return this.env.array(field, defaultValue);
  }

  public string(field: string, defaultValue = ""): string {
    return this.env.string(field, defaultValue);
  }

  public flag(flagName: string): boolean {
    return this.env.flag(flagName);
  }

  url(field: string, defaultValue = ""): string {
    return this.env.url(field, defaultValue);
  }
}
